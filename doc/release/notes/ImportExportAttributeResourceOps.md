## Import/Export Attribute Resource Operations

smtk::attribute::Import and smtk::attribute::Export operations import
and export attribute resources to/from XML-based SimBuilder files
(*.sbt, *.sbi).
