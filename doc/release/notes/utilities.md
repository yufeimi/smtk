# Changes to SMTK's utilities

+ Some python scripts have been added to `utilities/lldb`
  for tracing shared-pointer references. See the readme
  in that directory for more information.
